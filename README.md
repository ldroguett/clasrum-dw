# PROJECT CLASRUM

_[Proyecto UNAB]_

Clasrum es una pagina web de indole escolar desarrollada con el proposito de
poder ayudar en la en la gestion de Notas y Subida de material de apoyo de profesores, para colegios con escasos
recursos y que no cuenten con algun servicio online que les facilite esta tarea.
De esta manera los alumnos puedan ver su registro escolar y visualizar sus guias de manera rapida online.


©Sin Fines de lucro

## Comenzando 🚀

Start Point: 127.0.0.1:8000/home

#### ADMIN
- user: root
- pw: toor

#### TEACHER
- user: teacher1
- pw: toor

#### STUDENT
- user: student1
- pw: toor

_La asignacion de usuarios es manual via 127.0.0.1:8000/admin, ingresando con el superusuario root_
_La asignacion de usuarios debe ser para profesores y alumnos, de manera que puedan visualizar distintos elementos dependiendo del rol que se les otorgue_

#### Profesor puede: 📋 
- Editar todo lo respectivo a su curso asignado.
- Crear pruebas, subir su respectivo archivo.
- Crear guias, subir su respectivo archivo.
- Actualizar notas a cada alumno, con su respectiva prueba.

#### Alumno puede: 📋 
- Visualizar sus notas.
- Visualizar las pruebas envidas por el profesor.
- Descargar las pruebas.
- Visualizar las guias envidas por el profesor.
- Descargar las guias.
- Visualizar una lista de los profesores registrados, en caso de necesitar alguna información.
- Visualizar una lista de los compañeros registrados, en caso de necesitar alguna información.

#### Ambos pueden: 📋 
- Editar su perfil de usuario.
- Cambiar su contraseña (ingresando la contraseña anterior).
- Llenar un Formulario de Contacto, para solicitar alguna ayuda a los moderadores la Plataforma.

## Herramientas 📋
_Herramientas utilizadas en el uso del proyecto CLASRUM_

#### FRONTEND

* [BOOSTRAP 4](https://getbootstrap.com/docs/4.5/getting-started/download/) 
* [HTML5](https://developer.mozilla.org/es/docs/HTML/HTML5)
* [CSS3](https://www.w3schools.com/css/)
* [JQUERY](https://jquery.com)

#### BACKEND
* [DJANGO](https://www.djangoproject.com/download/)

#### BASE DE DATOS
* [SQLITE](https://www.sqlite.org/index.html)

#### VERSIONADORES

* [GIT](https://git-scm.com/downloads) 
* [GITLAB](https://about.gitlab.com) 

#### EXTRA

*[DOCKER](https://www.docker.com)



## Instalación 🔧

_Ir al repositorio [CLASRUM](https://gitlab.com/ldroguett/clasrum-dw) , luego hacer un [GIT CLONE](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)  luego de eso ...   _

_Para la ejecución de la pagina web CLASRUM debemos de seguir los siguientes pasos_

```
Ir a la carpeta donde se encuentra descargado el proyecto ,dar click derecho en el proyecto y ejecutar [GIT BASH HERE]

```

_Al iniciar la consola de GIT_

```
Ejecutar el comando 'Docker-Compose up -d '
```

_Seguido de ejecutar la linea_

```
Iniciar tu navegador de internet e ir a la ruta "127.0.0.1:8000/home"
```

_Al iniciar esa ruta inmediatamente te lleva al login de CLASRUM._



### Estilo de codificación ⌨️

_Explica que verifican estas pruebas y por qué_

```
El codigo esta escrito segun lo aprendido en diseño web de [DJANGO Documentation](https://docs.djangoproject.com/es/3.0/)

```


## Construido con 🛠️

_Framework usado y contenedor usado en este proyecto_

* [DJANGO 3 ](https://www.djangoproject.com/download/) FRAMEWORK
* [DOCKER](https://www.docker.com)



## Ayudas 📖
Seccion de paginas de ayuda para realizar el proyecto

* [FontAwesome](https://fontawesome.com/v3.2.1/icons/) iconos y botones
* [Django Documentation](https://docs.djangoproject.com/en/3.0/)

## Versionado 📌

Usamos [GIT](https://git-scm.com/downloads) y [GITLAB](https://about.gitlab.com) para seguir el proceso de documentacion de el proyecto

[Si desea ver el proceso de avance del proyecto](https://gitlab.com/ldroguett/clasrum-dw/-/commits/TestAddons)

## License
[©]Pagina de uso Libre. 

---
⌨️Creador por:  con ❤️ por [Kevin Salvador](https://gitlab.com/ksalvador1) , [Luis Droguett](https://gitlab.com/ldroguett) y [Nicolas Samula](https://gitlab.com/Nsamula)