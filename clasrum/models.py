from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Teacher
class Teacher(models.Model):
    Teacher_User = models.OneToOneField(User, null=True, blank=True, on_delete=models.SET_NULL)
    Teacher_First_Name = models.CharField(max_length=100)
    Teacher_Last_Name = models.CharField(max_length=100)
    Teacher_Rut = models.CharField(max_length=12, unique=True)
    Teacher_Date =models.DateField(null=True, blank=True)
    Teacher_Photo = models.ImageField(null=True, blank=True, upload_to='Teachers/')
    Teacher_Email = models.EmailField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{Teacher_First_Name} {Teacher_Last_Name}'.format(
            Teacher_First_Name=self.Teacher_First_Name, 
            Teacher_Last_Name=self.Teacher_Last_Name
            )

# Grade
class Grade(models.Model):
    Grade_Teacher = models.OneToOneField(Teacher, null=True, blank=True, on_delete = models.SET_NULL)
    Grade_Name = models.CharField(max_length=100)
    Grade_Code = models.CharField(max_length=20, unique=True)
    Grade_Logo = models.ImageField(null=True, blank=True, upload_to='Grades_Logo/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def count_student(self):
        return Student.objects.filter(Student_Grade=self.pk).count()

    def __str__(self):
        return '{Grade_Name} ({Grade_Code})'.format(
            Grade_Name=self.Grade_Name, 
            Grade_Code=self.Grade_Code
            )

#Subject
class Subject(models.Model):
    Subject_Grade = models.ForeignKey(Grade, on_delete=models.CASCADE)
    Type = (
    ("Maths","Maths"),
    ("Language","Language"),
    ("Science","Science"),
    ("History","History")
    )
    Subject_Type = models.CharField(choices=Type, max_length=20, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return '{Subject_Type} ({Subject_Grade})' . format(
            Subject_Type=self.Subject_Type, 
            Subject_Grade=self.Subject_Grade
            )

# Student
class Student(models.Model):
    Student_User = models.OneToOneField(User, null=True, blank=True, on_delete=models.SET_NULL)
    Student_Grade = models.ForeignKey(Grade, null=True, blank=True, on_delete=models.SET_NULL)
    Student_First_Name = models.CharField(max_length=100)
    Student_Last_Name = models.CharField(max_length=100)
    Student_Rut = models.CharField(max_length=12, unique=True)
    Student_Date =models.DateField(null=True, blank=True) 
    Student_Email = models.EmailField(max_length=100)
    Student_Photo = models.ImageField(null=True, blank=True, upload_to='Students/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{Student_First_Name}{Student_Last_Name} ({Student_Grade})' . format(
            Student_First_Name=self.Student_First_Name, 
            Student_Last_Name=self.Student_Last_Name, 
            Student_Grade=self.Student_Grade
            )

#Test
class Test(models.Model):
    Test_Subject = models.ForeignKey(Subject, on_delete = models.CASCADE)
    Test_Name = models.CharField(max_length=100)
    Test_File = models.FileField(null=True, blank=True, upload_to='Test_Files/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{Test_Name}({Test_Subject})'.format(
            Test_Name=self.Test_Name,
            Test_Subject=self.Test_Subject
            )

#TestScore
class TestScore(models.Model):
    TestScore_Test = models.ForeignKey(Test, on_delete = models.CASCADE)
    TestScore_Student = models.ForeignKey(Student, on_delete = models.CASCADE)
    TestScore_Score = models.DecimalField(max_digits=2, decimal_places=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{TestScore_Test} {TestScore_Student} {TestScore_Score}'.format(
            TestScore_Test=self.TestScore_Test,
            TestScore_Student=self.TestScore_Student,
            TestScore_Score=self.TestScore_Score
        )

#Guide
class Guide(models.Model):
    Guide_Subject = models.ForeignKey(Subject, on_delete = models.CASCADE)
    Guide_Name = models.CharField(max_length=100)
    Guide_File = models.FileField(null=True, blank=True, upload_to='Guide_Files/')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{Guide_Name}({Guide_Subject})'.format(
            Guide_Name=self.Guide_Name,
            Guide_Subject=self.Guide_Subject
            )