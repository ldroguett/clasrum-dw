from django.shortcuts import render, redirect
from clasrum.models import Teacher, Student, Grade, Subject, Test, TestScore, Guide
from clasrum.forms import TeacherForm, StudentForm, GradeForm, SubjectForm, TestForm, TestScoreForm, GuideForm
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required, permission_required
# Create your views here.

def home(request):
    template_name = 'home.html'
    data = {}
    data['title'] = 'Home'

    return render(request, template_name, data)





#TEACHER VIEWS
@permission_required('clasrum.add_teacher')
def teacher_create(request):
    data = {}
    template_name = 'teacher_create.html'
    data['title'] = 'Create Teacher'
    data['form'] = TeacherForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Teacher Created.'
                )
        return redirect('clasrum:teacher_list')

    return render(request, template_name, data)


@login_required()
def teacher_list(request):
    template_name = 'teacher_list.html'
    data = {}
    data['title'] = 'Teacher List'
    data['query'] = ''
    if 'q' in request.GET:
        data['query'] = request.GET.get('q')
        lists = Teacher.objects.filter(Teacher_Last_Name__icontains=data['query'])
    else:
        lists = Teacher.objects.all()

    page = request.GET.get('page', 1)
    paginator = Paginator(lists, 5)
    try:
        data['teachers'] = paginator.page(page)
    except PageNotAnInteger:
        data['teachers'] = paginator.page(1)
    except EmptyPage:
        data['teachers'] = paginator.page(paginator.num_pages)
    
    return render(request, template_name, data)


@permission_required('clasrum.change_teacher')
def teacher_update(request, teacher_ID):
    data = {}
    template_name = 'teacher_update.html'
    data['title'] = 'Update Teacher'
    teacher = Teacher.objects.get(id=teacher_ID)
    if request.method == 'POST':
        data['form'] = TeacherForm(request.POST, files=request.FILES, instance=teacher)
        if data['form'].is_valid():
            teacher.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Teacher Updated.'
                )
            return redirect('clasrum:teacher_list')
    else:
        data['form'] = TeacherForm(instance=teacher)

    return render(request, template_name, data)


@permission_required('clasrum.delete_teacher')
def teacher_delete(request, teacher_ID):
    data = {}
    template_name = 'teacher_delete.html'
    data['title'] = 'Delete Teacher'
    data['teacher'] = Teacher.objects.get(id=teacher_ID)
    if request.method == 'POST':
        data['teacher'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Teacher Deleted.'
                )
        return redirect('clasrum:teacher_list')

    return render(request, template_name, data)





#STUDENT VIEWS
@permission_required('clasrum.add_student')
def student_create(request):
    data = {}
    template_name = 'student_create.html'
    data['title'] = 'Create Student'
    data['form'] = StudentForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Student Created.'
                )
        return redirect('clasrum:student_list')

    return render(request, template_name, data)


@login_required()
def student_list(request):
    template_name = 'student_list.html'
    data = {}
    data['title'] = 'Student List'
    data['query'] = ''
    if 'q' in request.GET:
        data['query'] = request.GET.get('q')
        data['students'] = Student.objects.filter(Student_Last_Name__icontains=data['query'])
    else:
        data['students'] = Student.objects.all()
    g = Grade.objects.all().order_by('Grade_Name')  
    page = request.GET.get('page', 1)
    paginator = Paginator(g, 5)
    try:
        data['grades'] = paginator.page(page)
    except PageNotAnInteger:
        data['grades'] = paginator.page(1)
    except EmptyPage:
        data['grades'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


@permission_required('clasrum.change_student')
def student_update(request, student_ID):
    data = {}
    template_name = 'student_update.html'
    data['title'] = 'Update Student'
    student = Student.objects.get(id=student_ID)
    if request.method == 'POST':
        data['form'] = StudentForm(request.POST, files=request.FILES, instance=student)
        if data['form'].is_valid():
            student.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Student Updated.'
                )
            return redirect('clasrum:student_list')
    else:
        data['form'] = StudentForm(instance=student)

    return render(request, template_name, data)


@permission_required('clasrum.delete_student')
def student_delete(request, student_ID):
    data = {}
    template_name = 'student_delete.html'
    data['title'] = 'Delete Student'
    data['student'] = Student.objects.get(id=student_ID)
    if request.method == 'POST':
        data['student'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Student Deleted.'
                )
        return redirect('clasrum:student_list')

    return render(request, template_name, data)





#GRADE VIEWS
@permission_required('clasrum.add_grade')
def grade_create(request):
    data = {}
    template_name = 'grade_create.html'
    data['title'] = 'Create Grade'
    data['form'] = GradeForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Grade Created.'
                )
        return redirect('clasrum:grade_list')

    return render(request, template_name, data)


@login_required()
def grade_list(request):
    template_name = 'grade_list.html'
    data = {}
    data['title'] = 'Grade List'
    data['query'] = ''
    if 'q' in request.GET:
        data['query'] = request.GET.get('q')
        lists = Grade.objects.filter(Grade_Name__icontains=data['query'])
    else:
        lists = Grade.objects.all()

    page = request.GET.get('page', 1)
    paginator = Paginator(lists, 5)
    try:
        data['grades'] = paginator.page(page)
    except PageNotAnInteger:
        data['grades'] = paginator.page(1)
    except EmptyPage:
        data['grades'] = paginator.page(paginator.num_pages)
    
    return render(request, template_name, data)


@permission_required('clasrum.change_grade')
def grade_update(request, grade_ID):
    data = {}
    template_name = 'grade_update.html'
    data['title'] = 'Update Grade'
    grade = Grade.objects.get(id=grade_ID)
    if request.method == 'POST':
        data['form'] = GradeForm(request.POST, files=request.FILES, instance=grade)
        if data['form'].is_valid():
            grade.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Grade Updated.'
                )
            return redirect('clasrum:grade_list')
    else:
        data['form'] = GradeForm(instance=grade)

    return render(request, template_name, data)


@permission_required('clasrum.delete_grade')
def grade_delete(request, grade_ID):
    data = {}
    template_name = 'grade_delete.html'
    data['title'] = 'Delete grade'
    data['grade'] = Grade.objects.get(id=grade_ID)
    if request.method == 'POST':
        data['grade'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Grade Deleted.'
                )
        return redirect('clasrum:grade_list')

    return render(request, template_name, data)





#SUBJECT VIEWS
@permission_required('clasrum.add_subject')
def subject_create(request):
    data = {}
    template_name = 'subject_create.html'
    data['title'] = 'Create Subject'
    data['form'] = SubjectForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Subject Created.'
                )
        return redirect('clasrum:subject_list')

    return render(request, template_name, data)

@login_required()
def subject_list(request):
    template_name = 'subject_list.html'
    data = {}
    data['title'] = 'Subject List'
    data['subjects'] = Subject.objects.all()
    data['grades'] = Grade.objects.all().order_by('Grade_Name')  

    return render(request, template_name, data)


@permission_required('clasrum.change_subject')
def subject_update(request, subject_ID):
    data = {}
    template_name = 'subject_update.html'
    data['title'] = 'Update Subject'
    subject = Subject.objects.get(id=subject_ID)
    if request.method == 'POST':
        data['form'] = SubjectForm(request.POST, files=request.FILES, instance=subject)
        if data['form'].is_valid():
            Subject.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Subject Updated.'
                )
            return redirect('clasrum:subject_list')
    else:
        data['form'] = SubjectForm(instance=subject)

    return render(request, template_name, data)


@permission_required('clasrum.delete_subject')
def subject_delete(request, subject_ID):
    data = {}
    template_name = 'subject_delete.html'
    data['title'] = 'Delete Subject'
    data['subject'] = Subject.objects.get(id=subject_ID)
    if request.method == 'POST':
        data['subject'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Subject Deleted.'
                )
        return redirect('clasrum:subject_list')

    return render(request, template_name, data)





#TEST VIEWS
@permission_required('clasrum.add_test')
def test_create(request):
    data = {}
    template_name = 'test_create.html'
    data['title'] = 'Create Test'
    data['form'] = TestForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Test Created.'
                )
        return redirect('clasrum:test_list')

    return render(request, template_name, data)


@login_required()
def test_list(request):
    template_name = 'test_list.html'
    data = {}
    data['title'] = 'Test List'
    data['tests'] = Test.objects.all()
    data['subjects'] = Subject.objects.all()
    data['grades'] = Grade.objects.all().order_by('Grade_Name')  

    return render(request, template_name, data)


@permission_required('clasrum.change_test')
def test_update(request, test_ID):
    data = {}
    template_name = 'test_update.html'
    data['title'] = 'Update Test'
    test = Test.objects.get(id=test_ID)
    if request.method == 'POST':
        data['form'] = TestForm(request.POST, files=request.FILES, instance=test)
        if data['form'].is_valid():
            test.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Test Updated.'
                )
            return redirect('clasrum:test_list')
    else:
        data['form'] = TestForm(instance=test)

    return render(request, template_name, data)


@permission_required('clasrum.delete_test')
def test_delete(request, test_ID):
    data = {}
    template_name = 'test_delete.html'
    data['title'] = 'Delete Test'
    data['test'] = Test.objects.get(id=test_ID)
    if request.method == 'POST':
        data['test'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Test Deleted.'
                )
        return redirect('clasrum:test_list')

    return render(request, template_name, data)





#TESTSCORE VIEWS
@permission_required('clasrum.add_testscore')
def testscore_create(request):
    data = {}
    template_name = 'testscore_create.html'
    data['title'] = 'Create Test Score'
    data['form'] = TestScoreForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Test Score Created.'
                )
        return redirect('clasrum:testscore_list')

    return render(request, template_name, data)


@login_required()
def testscore_list(request):
    template_name = 'testscore_list.html'
    data = {}
    data['title'] = 'Test Score List'
    data['testscores'] = TestScore.objects.all()
    data['subjects'] = Subject.objects.all()
    data['students'] = Student.objects.all()
    data['grades'] = Grade.objects.all().order_by('Grade_Name')  

    return render(request, template_name, data)


@permission_required('clasrum.change_testscore')
def testscore_update(request, testscore_ID):
    data = {}
    template_name = 'testscore_update.html'
    data['title'] = 'Update Test Score'
    testscore = TestScore.objects.get(id=testscore_ID)
    if request.method == 'POST':
        data['form'] = TestScoreForm(request.POST, files=request.FILES, instance=testscore)
        if data['form'].is_valid():
            testscore.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Test Score Updated.'
                )
            return redirect('clasrum:testscore_list')
    else:
        data['form'] = TestScoreForm(instance=testscore)

    return render(request, template_name, data)


@permission_required('clasrum.delete_testscore')
def testscore_delete(request, testscore_ID):
    data = {}
    template_name = 'testscore_delete.html'
    data['title'] = 'Delete Test Score'
    data['testscore'] = TestScore.objects.get(id=testscore_ID)
    if request.method == 'POST':
        data['testscore'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Test Score Deleted.'
                )
        return redirect('clasrum:testscore_list')

    return render(request, template_name, data)





#GUIDE VIEWS
@permission_required('clasrum.add_guide')
def guide_create(request):
    data = {}
    template_name = 'guide_create.html'
    data['title'] = 'Create Guide'
    data['form'] = GuideForm(request.POST or None)
    if data['form'].is_valid():
        data['form'].save()
        messages.add_message(
                    request,
                    messages.SUCCESS,
                    'Guide Created.'
                )
        return redirect('clasrum:home')

    return render(request, template_name, data)


@login_required()
def guide_list(request):
    template_name = 'guide_list.html'
    data = {}
    data['title'] = 'Guide List'
    data['guides'] = Guide.objects.all()
    data['subjects'] = Subject.objects.all()
    data['grades'] = Grade.objects.all().order_by('Grade_Name')  

    return render(request, template_name, data)


@permission_required('clasrum.change_guide')
def guide_update(request, guide_ID):
    data = {}
    template_name = 'guide_update.html'
    data['title'] = 'Update Guide'
    guide = Guide.objects.get(id=guide_ID)
    if request.method == 'POST':
        data['form'] = GuideForm(request.POST, files=request.FILES, instance=guide)
        if data['form'].is_valid():
            guide.save()
            messages.add_message(
                    request,
                    messages.INFO,
                    'Guide Updated.'
                )
            return redirect('clasrum:guide_list')
    else:
        data['form'] = GuideForm(instance=guide)

    return render(request, template_name, data)


@permission_required('clasrum.delete_guide')
def guide_delete(request, guide_ID):
    data = {}
    template_name = 'guide_delete.html'
    data['title'] = 'Delete Guide'
    data['guide'] = Guide.objects.get(id=guide_ID)
    if request.method == 'POST':
        data['guide'].delete()
        messages.add_message(
                    request,
                    messages.ERROR,
                    'Guide Deleted.'
                )
        return redirect('clasrum:guide_list')

    return render(request, template_name, data)




@login_required()
def mytestscores(request):
    template_name = 'mytestscores.html'
    data = {}
    data['title'] = 'My Tests Score'
    data['testscores'] = TestScore.objects.all()
    data['subjects'] = Subject.objects.all()

    return render(request, template_name, data)

@login_required()
def mytests(request):
    template_name = 'mytests.html'
    data = {}
    data['title'] = 'My Tests'
    data['tests'] = Test.objects.all()
    data['subjects'] = Subject.objects.all()

    return render(request, template_name, data)