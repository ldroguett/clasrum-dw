from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from clasrum import views

app_name = "clasrum"

urlpatterns = [    
    path('home', views.home, name='home'),

    path('teacher/list', views.teacher_list, name='teacher_list'),
    path('teacher/create', views.teacher_create, name='teacher_create'),
    path('teacher/update/<int:teacher_ID>', views.teacher_update, name='teacher_update'),
    path('teacher/delete/<int:teacher_ID>', views.teacher_delete, name='teacher_delete'),

    path('student/list', views.student_list, name='student_list'),
    path('student/create', views.student_create, name='student_create'),
    path('student/update/<int:student_ID>', views.student_update, name='student_update'),
    path('student/delete/<int:student_ID>', views.student_delete, name='student_delete'),    

    path('grade/list', views.grade_list, name='grade_list'),
    path('grade/create', views.grade_create, name='grade_create'),
    path('grade/update/<int:grade_ID>', views.grade_update, name='grade_update'),
    path('grade/delete/<int:grade_ID>', views.grade_delete, name='grade_delete'),    

    path('subject/list', views.subject_list, name='subject_list'),
    path('subject/create', views.subject_create, name='subject_create'),
    path('subject/update/<int:subject_ID>', views.subject_update, name='subject_update'),
    path('subject/delete/<int:subject_ID>', views.subject_delete, name='subject_delete'),

    path('test/list', views.test_list, name='test_list'),
    path('test/create', views.test_create, name='test_create'),
    path('test/update/<int:test_ID>', views.test_update, name='test_update'),
    path('test/delete/<int:test_ID>', views.test_delete, name='test_delete'),

    path('testscore/list', views.testscore_list, name='testscore_list'),
    path('testscore/create', views.testscore_create, name='testscore_create'),
    path('testscore/update/<int:testscore_ID>', views.testscore_update, name='testscore_update'),
    path('testscore/delete/<int:testscore_ID>', views.testscore_delete, name='testscore_delete'),

    path('mytestscores/', views.mytestscores, name='mytestscores'),
    path('mytests/', views.mytests, name='mytests'),

    path('guide/list', views.guide_list, name='guide_list'),
    path('guide/create', views.guide_create, name='guide_create'),
    path('guide/update/<int:guide_ID>', views.guide_update, name='guide_update'),
    path('guide/delete/<int:guide_ID>', views.guide_delete, name='guide_delete'),    
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)