from django import forms
from clasrum.models import Teacher, Grade, Student, Subject, Test, TestScore, Guide

class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = (
            'Teacher_First_Name',
            'Teacher_Last_Name',
            'Teacher_Rut',
            'Teacher_Date',
            'Teacher_Email',
            'Teacher_Photo'
        )

class GradeForm(forms.ModelForm):
    class Meta:
        model = Grade
        fields = (
            'Grade_Teacher',
            'Grade_Name',
            'Grade_Code',
            'Grade_Logo'
        )
        
class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = (
            'Subject_Grade',
            'Subject_Type',
        )

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = (
            'Student_Grade',
            'Student_First_Name',
            'Student_Last_Name',
            'Student_Rut',
            'Student_Date',
            'Student_Email',
            'Student_Photo'
        )

class TestForm(forms.ModelForm):
    class Meta:
        model = Test
        fields = (
            'Test_Subject',
            'Test_Name',
            'Test_File'
        )

class TestScoreForm(forms.ModelForm):
    class Meta:
        model = TestScore
        fields = (
            'TestScore_Test',
            'TestScore_Student',
            'TestScore_Score'
        )

class GuideForm(forms.ModelForm):
    class Meta:
        model = Guide
        fields = (
            'Guide_Subject',
            'Guide_Name',
            'Guide_File'
        )

