from django.contrib import admin
from .models import Teacher, Student, Grade, Subject, Test, TestScore, Guide
from django.utils.html import mark_safe

# Register your models here.
@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    pass

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    pass

@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    pass

@admin.register(Grade)
class GradeAdmin(admin.ModelAdmin):
    pass

@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    pass

@admin.register(TestScore)
class TestScoreAdmin(admin.ModelAdmin):
    pass

@admin.register(Guide)
class GuideAdmin(admin.ModelAdmin):
    pass