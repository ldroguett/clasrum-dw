from django.urls import path
from auth_login import views

app_name = 'auth'

urlpatterns = [
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('register', views.register, name='register'),
    path('change_pw', views.change_pw, name='change_pw'),
    path('edit_user', views.edit_user, name='edit_user'),
]