from django.shortcuts import render, redirect
from django.contrib import auth, messages
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from auth_login.forms import UserClasrumForm, EditUserForm

def login(request):
    data = {}
    template_name = 'login.html'
    data['title'] = 'Clasrum - Login'

    auth.logout(request)

    if request.POST:
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(
            username=username,
            password=password
        )
        if user is not None:
            if user.is_active:
                auth.login(request, user)
                return redirect('clasrum:home')

            else:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'User or Password Wrong.'
                )
        else:
            messages.add_message(
                request,
                messages.ERROR,
                'User or Password Wrong.'
            )

    return render(request, template_name, data)

def logout(request):
    auth.logout(request)
    return redirect('clasrum:home')

def register(request):
    data = {}
    template_name = 'register.html'
    data['title'] = 'Clasrum - Register'
    data['form'] = UserClasrumForm(request.POST or None)

    if request.method == 'POST':
        if data['form'].is_valid():
            try:
                user = User.objects.create_user(
                    username=request.POST['username'],
                    password=request.POST['password'],
                )

                user.save()

                messages.add_message(
                  request,
                  messages.SUCCESS,
                  'User Created.'
                )

            except IntegrityError as ie:
                messages.add_message(
                  request,
                  messages.ERROR,
                  'Error while creating user ERROR: {error}.' .format(error=str(ie))
                )

            except ValidationError as ve:
                messages.add_message(
                  request,
                  messages.ERROR,
                  'Problems with form validation: {error}.' .format(error=str(ie))
                )

            return redirect('auth:login')

        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Error while creating user.'
            )

    return render(request, template_name, data)

@login_required()
def edit_user(request):
    data = {}
    template_name = 'edit_user.html'
    data['title'] = 'Update User'
    if request.method == 'POST':
        data['form'] = EditUserForm(request.POST, instance=request.user)
        if data['form'].is_valid():
            try:
                data['form'].save()
                messages.add_message(
                            request,
                            messages.INFO,
                            'User Updated.'
                        )
                return redirect('clasrum:home')

            except IntegrityError as ie:
                messages.add_message(
                  request,
                  messages.ERROR,
                  'Error while editing user ERROR: {error}.' .format(error=str(ie))
                )

            except ValidationError as ve:
                messages.add_message(
                  request,
                  messages.ERROR,
                  'Problems with form validation: {error}.' .format(error=str(ie))
                )

        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Error while editing user.'
                )
    else:
        data['form'] = EditUserForm(instance=request.user)

    return render(request, template_name, data)

@login_required()
def change_pw(request):
    data = {}
    template_name = 'change_pw.html'
    data['title'] = 'Update User'
    if request.method == 'POST':
        data['form'] = PasswordChangeForm(data=request.POST, user=request.user)
        if data['form'].is_valid():
            try:
                data['form'].save()
                messages.add_message(
                            request,
                            messages.INFO,
                            'User Updated.'
                        )
                return redirect('clasrum:home')

            except IntegrityError as ie:
                messages.add_message(
                  request,
                  messages.ERROR,
                  'Error while changing password ERROR: {error}.' .format(error=str(ie))
                )

            except ValidationError as ve:
                messages.add_message(
                  request,
                  messages.ERROR,
                  'Problems with form validation: {error}.' .format(error=str(ie))
                )

        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Error while changing password.'
                )
    else:
        data['form'] = PasswordChangeForm(user=request.user)

    return render(request, template_name, data)