def user_data(request):

    try:
        user = request.user
        fullname = '{first_name} {last_name}'.format(
            first_name=user.first_name, 
            last_name=user.last_name
        )

        data = {
            'id': user.id,
            'name': user.username,
            'fullname': fullname,
            'user_type': 'ADMIN' if user.is_superuser else 'USER',
        } 
        
    except AttributeError as e:
        data = {}


    return data
