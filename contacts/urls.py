from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from contacts import views

app_name = "contacts"

urlpatterns = [
    path('send_email/', views.send_email, name='send_email'),
]
