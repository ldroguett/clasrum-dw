from django.shortcuts import render
from contacts.forms import ContactForm
from django.contrib import messages
from django.core.mail import send_mail, BadHeaderError
from django.conf import settings
from django.core.mail import EmailMultiAlternatives


# Create your views here.
def send_email(request):
    template_name = 'form_contact.html'
    data = {}

    form = ContactForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():

            subject = request.POST['subject']
            text_content = request.POST['message']
            from_email = request.POST['from_email']
            html_content = '<h3>Contact Form</h3><p>{0}</p>'.format(text_content)

            try:
                msg = EmailMultiAlternatives(
                    subject,
                    text_content,
                    from_email,
                    [settings.EMAIL_CONTACT_DEFAULT]
                )
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                
            except BadHeaderError:
                messages.add_message(
                    request,
                    messages.ERROR,
                    'Invalid header found'
                )

            messages.add_message(
                request,
                messages.SUCCESS,
                'Your query has been sent successfully!'
            )

        else:
            messages.add_message(
                request,
                messages.ERROR,
                'Problems sending query'
            )

    form = ContactForm()
    data['form'] = form
    return render(request, template_name, data)
